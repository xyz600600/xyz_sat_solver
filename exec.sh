#!/bin/bash

data_path="data/75-325u/"
# data_path="data/75-325/"
# data_path="data/50-218u/"
# data_path="data/50-218/"
# data_path="data/20-91/"
result_dir="result"

if [ ! -e $result_dir ]; then
    mkdir $result_dir
fi

for input_file in `ls $data_path`
do
    ./target/release/xyz_sat_solver "`pwd`/$data_path$input_file" "$result_dir/$input_file"
    echo $input_file
done
