use util::*;

struct ProblemState {

    // i番目の変数が更新された際、どの節を見に行く必要があるか
    cnf_index_list: Vec<Vec<(usize, usize)>>,
}

impl ProblemState {
    pub fn new(prob: &Problem) -> ProblemState {
        let mut lst = vec![vec![]; prob.data.len()];

        for (idx, cnf) in prob.data.iter().enumerate() {
            for (i, var) in cnf.data.iter().enumerate() {
                lst[var.index].push((idx, i));
            }
        }

        ProblemState {
            cnf_index_list: lst,
        }
    }
}

fn select_moms(prob: &Problem, sol: &Solution) -> usize {
    let mut freqs = vec![0usize; prob.data.len()];

    let min_cnf_len = prob.data.iter().filter_map(|item: &CNF| {
        if item.get_undecided_variable(sol) > 0 {
            Some(item)
        } else {
            None
        }
    }).map(|item: &CNF| item.data.len()).min().unwrap();

    for cnf in &prob.data {
        if cnf.data.len() == min_cnf_len {
            for var in &cnf.data {
                if sol.variable_array[var.index] == State::UNKNOWN {
                    freqs[var.index] += 1;
                }
            }
        }
    }
    freqs.iter().enumerate().max_by_key(|val| val.1).unwrap().0
}

fn get_freqent_state(prob: &Problem, idx: usize, state: &ProblemState, sol: &Solution) -> State {
    let mut true_count = 0;
    let mut false_count = 0;

    for &(cnf_idx, var_idx) in &state.cnf_index_list[idx] {
        if prob.data[cnf_idx].get_undecided_variable(sol) > 0 {
            if prob.data[cnf_idx].data[var_idx].state == State::TRUE {
                true_count += 1;
            } else {
                false_count += 1;
            }
        }
    }
    if true_count > false_count {
        State::TRUE
    } else {
        State::FALSE
    }
}

fn unit_propagate(prob: &Problem, sol: &mut Solution, problem_state: &ProblemState, propagated: &mut Vec<usize>, init_var_idx: usize) {

    let mut stack = vec![init_var_idx];

    // TODO: 容易に高速化できる
    while let Some(idx) = stack.pop() {
        for &(cnf_idx, _) in &problem_state.cnf_index_list[idx] {
            if let Some(prop_idx) = prob.data[cnf_idx].has_unit_propagate(sol) {
                if sol.variable_array[prop_idx] == State::UNKNOWN {
                    let prop_var_idx = prob.data[cnf_idx].data[prop_idx].index;
                    propagated.push(prop_var_idx);
                    sol.variable_array[prop_var_idx] = prob.data[cnf_idx].data[prop_idx].state;
                    sol.undecided_variable -= 1;

                    stack.push(prop_var_idx);
                }
            }
        }
    }
}

fn dfs(prob: &Problem, sol: &mut Solution, problem_state: &ProblemState, depth: usize) -> bool {

    if sol.undecided_variable == 0 {
        verify(prob, sol)
    } else {
        let idx = select_moms(&prob, &sol);
        let init_state = get_freqent_state(prob, idx, problem_state, sol);
        sol.variable_array[idx] = init_state;
        sol.undecided_variable -= 1;

        let mut propagated_variable: Vec<usize> = vec![];
        unit_propagate(prob, sol, problem_state, &mut propagated_variable, idx);

        if !prob.unsat(sol) && dfs(prob, sol, problem_state, depth + 1) {
            true
        } else {
            sol.variable_array[idx] = init_state.opposite();
            for unit in propagated_variable.iter() {
                sol.variable_array[*unit] = State::UNKNOWN;
            }
            sol.undecided_variable += propagated_variable.len();
            propagated_variable.clear();
            unit_propagate(prob, sol, problem_state, &mut propagated_variable, idx);

            if !prob.unsat(sol) && dfs(prob, sol, problem_state, depth + 1) {
                true 
            } else {
                sol.variable_array[idx] = State::UNKNOWN;
                sol.undecided_variable += 1;

                for unit in propagated_variable.iter() {
                    sol.variable_array[*unit] = State::UNKNOWN;
                }
                sol.undecided_variable += propagated_variable.len();

                false
            }
        }
    }
}

pub fn solve(prob: &Problem, sol: &mut Solution) -> bool {
    let problem_state = ProblemState::new(&prob);
    dfs(prob, sol, &problem_state, 0)
}