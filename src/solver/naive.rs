
use util::*;

pub fn solve(prob: &Problem, sol: &mut Solution) -> bool {
    for i in 0..(1usize << sol.len()) {
        for j in 0..sol.len() {
            sol.variable_array[j] = if (i & (1usize << j)) == 0usize {
                State::TRUE
            } else {
                State::FALSE
            }
        }
        if verify(prob, sol) {
            return true;
        }
    }
    false
}