extern crate regex;

use std::env;

mod solver;

mod util;
use ::util::*;

fn main() {
    let args = env::args().collect::<Vec<String>>();

    let prob = Problem::load(args[1].as_str());
    let mut sol = Solution::new(prob.dimension);
    let success = solver::dpll::solve(&prob, &mut sol);
    // let success = solver::naive::solve(&prob, &mut sol);

    let output_path = &args[2];
    output_result(sol, success, output_path);
}