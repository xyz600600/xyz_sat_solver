
use std::fs::File;
use std::path::Path;
use std::io::{BufRead, BufReader, Write, BufWriter};

use regex::Regex;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum State {
    UNKNOWN,
    TRUE, 
    FALSE,
}

impl State {
    fn from(positive: bool) -> State {
        if positive {
            State::TRUE
        } else {
            State::FALSE
        }
    }

    pub fn opposite(&self) -> State {
        match *self {
            State::TRUE => State::FALSE,
            State::FALSE => State::TRUE,
            State::UNKNOWN => State::UNKNOWN,
        }
    }
}

#[derive(Debug)]
pub struct Solution {
    pub variable_array: Vec<State>,
    pub undecided_variable: usize,
}

impl Solution {
    pub fn new(dim: usize) -> Solution {
        Solution {
            variable_array: vec![State::UNKNOWN; dim],
            undecided_variable: dim,
        }
    }

    pub fn len(&self) -> usize {
        self.variable_array.len()
    }
}

#[derive(Debug)]
pub struct Variable {
    pub index: usize,
    pub state: State,
}

impl Variable {
    pub fn new(index: usize, state: State) -> Variable {
        Variable {
            state: state,
            index: index
        }
    }
}

#[derive(Debug)]
pub struct CNF {
    pub data: Vec<Variable>
}

impl CNF {
    pub fn new() -> CNF {
        CNF {
            data: vec![]
        }
    }

    pub fn push(&mut self, item: Variable) {
        self.data.push(item);
    }

    // 未確定な変数の個数を取得
    pub fn get_undecided_variable(&self, sol: &Solution) -> usize {
        self.data.iter().map(|var| sol.variable_array[var.index] == State::UNKNOWN).count()
    }

    pub fn unsat(&self, sol: &Solution) -> bool {
        for var in &self.data {
            if sol.variable_array[var.index] == State::UNKNOWN || sol.variable_array[var.index] == var.state {
                return false;
            }
        }
        true
    }

    // cnf内で、どの var が propagate 対象であるか
    pub fn has_unit_propagate(&self, sol: &Solution) -> Option<usize> {
        let mut index = self.data.len();
        let mut undecided_counter = 0;
        let mut correct_counter = 0;

        for (pos, var) in self.data.iter().enumerate() {
            if sol.variable_array[var.index] == State::UNKNOWN {
                index = pos;
                undecided_counter += 1;
            }
            if var.state == sol.variable_array[var.index] {
                correct_counter += 1;
            }
        }
        if undecided_counter == 1 && correct_counter == 0 {
            Some(index)
        } else {
            None
        }
    }
}

#[derive(Debug)]
pub struct Problem {
    pub data: Vec<CNF>,
    pub dimension: usize,
}

impl Problem {

    fn get_cnf(clause_data: &mut Vec<i64>, index: &mut usize) -> CNF {
        let mut cnf = CNF::new();
        while *index < clause_data.len() {
            let v = clause_data[*index];
            *index += 1;
            if v == 0 {
                break;
            } else {
                let val = Variable::new((v.abs() - 1) as usize, State::from(v > 0));
                cnf.push(val);
            }
        }
        cnf
    }

    fn new() -> Problem {
        Problem {
            data: vec![],
            dimension: 0,
        }
    }

    pub fn load(path: &str) -> Problem {
        let path = Path::new(path);
        let fin = BufReader::new(File::open(path).unwrap());

        let mut problem = Problem::new();

        let mut num_clause = 0;
        let mut clause_data: Vec<i64> = vec![];

        let pat = Regex::new("[ \t]+").unwrap();

        for line in fin.lines().map(|l| l.unwrap()) {
            let splited = pat.split(&line.trim()).collect::<Vec<&str>>();
            match splited[0] {
                "c" => {},
                "p" => {
                    problem.dimension = splited[2].parse::<usize>().unwrap();
                    num_clause = splited[3].parse::<usize>().unwrap();
                },
                "%" => break,
                _ => {
                    for item in splited {
                        let value = item.parse::<i64>().unwrap();
                        clause_data.push(value);
                    }
                }
            }
        }
        let mut index = 0;
        while index < clause_data.len() {
            let cnf = Problem::get_cnf(&mut clause_data, &mut index);
            problem.data.push(cnf);
        }
        assert_eq!(num_clause, problem.data.len());
        problem
    }

    pub fn unsat(&self, sol: &Solution) -> bool {
        for cnf in &self.data {
            if cnf.unsat(sol) {
                return true;
            }
        }
        false
    }
}

pub fn verify(prob: &Problem, sol: &mut Solution) -> bool {
    for cnf in &prob.data {
        let mut success = false;
        for var in &cnf.data {
            if var.state == sol.variable_array[var.index] {
                success = true;
                break;
            }
        }
        if !success {
            return false;
        }
    }
    true
}

pub fn output_result(sol: Solution, success: bool, output_path: &str) {
    let mut fout = BufWriter::new(File::create(output_path).unwrap());

    if success {
        fout.write(b"SAT\n").unwrap();
        for i in 0..sol.variable_array.len() {
            if i > 0 {
                fout.write(b" ").unwrap();
            }
            let val = if sol.variable_array[i] == State::TRUE {
                (i + 1) as i64
            } else {
                -((i + 1) as i64)
            };
            fout.write(&val.to_string().as_str().as_bytes()).unwrap();
        }
        fout.write(b"\n").unwrap();
    } else {
        fout.write(b"UNSAT").unwrap();
    }
}