#!/bin/bash

data_path="data/75-325/"
# data_path="data/50-218/"
# data_path="data/20-91/"
# data_path="data/test/"

for input_file in `ls $data_path`
do
    echo "`pwd`/$data_path$input_file"
    # RUST_BACKTRACE=1 ./target/debug/xyz_sat_solver "`pwd`/$data_path$input_file" result.cnf
    ./target/release/xyz_sat_solver "`pwd`/$data_path$input_file" result.cnf
    break
done
